function redireccionarPagina() {
    location.href = "/";
}

function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function soloNumeros(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}
$(document).ready(function() {
    jQuery.validator.addMethod('email_rule', function(value, element) {
        if (/^([a-zA-Z0-9_\-\.]+)\+?([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value)) {
            return true;
        } else {
            return false;
        };
    });
    $('#contact').validate({
        rules: {
            'email': {
                required: true,
                email_rule: true
            },
            'nombre': {
                required: true
            },
            'mensaje': {
                required: true
            },
            'telefono': {
                required: true
            },
            'fecha': {
                required: true
            },
        },
        messages: {
            'email': {
                required: "El ingreso del email es requerido",
                email_rule: "Ingrese un email valido"
            },
            'nombre': {
                required: "El nombre del nombre es requerido"
            },
            'mensaje': {
                required: "El ingreso del mensaje es requerido"
            },
            'telefono': {
                required: "El número de telefono es requerido"
            },
            'fecha': {
                required: "Por favor recarga la página"
            },
        },
        submitHandler: function(form) {
            swal({
                title: "Verificando...",
                text: "Por favor espere",
                icon: "success",
                button: false,
                showConfirmButton: false,
                allowOutsideClick: false,
                closeOnClickOutside: false,
            });
            $.ajax({
                type: $(form).attr('method'),
                url: $(form).attr('action'),
                data: $(form).serializeArray(),
                success: function(response) {
                    $('#contact')[0].reset();
                    swal("Tu correo a sido enviado", "En unos momentos nos contactaremos contigo", "success");
                    setTimeout("redireccionarPagina()", 2000);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal("Upps a ocurido un error", "Por favor intentalo mas tarde", "warning");
                }
            });
        }
    })
});
