
<html>
    <head>
        <meta charset="UTF-8">
        <title>Nunkui Contacto</title>
    </head>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="padding: 0px 0 30px 0;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                        <tr>
                            <td style="padding: 0px ; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif; background-color: rgb(51, 193, 207);">
                                <img src="assets/img/titulo.png" alt="Nunkui" width="200" height="100" style="display: block;" />
                                <center><span style="color:white ; text-align:center ;font-size:38px !important">  Nuevo Contacto  </span></center>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                            <b> Información de contacto </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 40px;">
    
    
                                            <b style="padding-top:90px "> Nombre: </b> {!! $nombre !!}
                                            <br/>
                                            <b style="padding-top:90px "> Email: </b> {!! $email !!}
                                            <br/>
                                            <b style="padding-top:90px"> Teléfono: </b> {!! $telefono !!}
                                            <br/>
                                            <b style="padding-top:90px"> Mensaje: </b> {!! $mensaje !!}
                                            <br/>
                                            <b style="padding-top:90px">Fecha: </b> {!! $fecha !!}
                                            <br/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
    
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 30px 30px 30px 30px; background-color: rgb(51, 193, 207);">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="text-align: center; color: rgb(255, 255, 255);background: transparent; font-family: Arial, sans-serif; font-size: 14px;" width="100%">
                                            © Nunkui.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    </html>