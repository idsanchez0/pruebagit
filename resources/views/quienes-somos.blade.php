<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--
Tinker Template
http://www.templatemo.com/tm-506-tinker
-->
        <title>Nunkui</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Yanone+Kaffeesatz:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">

        <link href="assets/img/favicon.png" rel="icon">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/fontAwesome.css">
        <link rel="stylesheet" href="assets/css/hero-slider.css">
        <link rel="stylesheet" href="assets/css/owl-carousel.css">
        <link rel="stylesheet" href="assets/css/templatemo-style.css">
        <link rel="stylesheet" href="assets/css/lightbox.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            .baner-content {
                width: 100%;
                padding-top: 35vh;
                text-align: center;
                background-image: url(assets/img/quienes-somos.png);
                vertical-align: middle;
            }
            #about1 {
                width: 100%;
                padding: 150px 0px;
                background-image: url(assets/img/datos_empresa.png);
                background-attachment: fixed;
            }
            #about {
                padding: 150px 0px;
                background-image: url(assets/img/historia.png);
                background-repeat: no-repeat;
                background-size: cover;
                background-attachment: fixed;
            }
            #idio{
                width: 4px
            }
            #idio1{
                width: 4px;
            }
            @media(max-width:720px){
                #about1 {
                width: 100%;
                padding: 150px 0px;
                background-image: url(assets/img/datos_empresa1.png);
                background-attachment: fixed;
            }
            #about {
                padding: 150px 0px;
                background-image: url(assets/img/historia1.png);
                background-repeat: no-repeat;
                background-size: cover;
                background-attachment: fixed;
                }
            }
        </style>
    </head>

<body>
    <div class="header">
        <div class="container">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand scroll-top"><em>Nun</em>kui</a>
                </div>
                <!--/.navbar-header-->
                <div id="main-nav" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/"><b class="lang" key="home">Inicio</b></a></li>
                        <li><a href="/productos"><b class="lang" key="product">Productos</b></a></li>
                        <li><a href="/galeria"><b class="lang" key="galery">Galeria</b></a></li>
                        <li><a href="/quienes-somos"><b class="lang" key="who">Quienes Somos</b></a></li>
                        <li><a href="/contacto"><b class="lang" key="contact">Contacto</b></a></li>
                        <li><img class="translate lang" key="es" id="es" src="assets/img/esp.png"></li>
                        <li><img class="translate lang" key="en" id="en" src="assets/img/eu.png"></li>
                    </ul>
                </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
        <!--/.container-->
    </div>
    <!--/.header-->


    <div class="parallax-content baner-content" id="home">
        <div class="container">
            <div id="btn-quienes-somos" class="text-content">
                <h2 style="font-family: Yanone Kaffeesatz !important; font-size: 65px; color: #fff">NUN<span style="font-family: Yanone Kaffeesatz !important; color: #33c1cf">KUI</span></h2>
                <br />
                <div id="quienes" style="text-align: center">
                    <p class="lang" key="desc1" style="font-size: 22px; line-height: 140%; ">Somos una empresa que se dedica a la producción y comercialización de
                        Pitahaya Amarilla, con un arraigado compromiso social
                        y ambiental, buscando aportar valor en cada uno de los eslabones de nuestra
                        cadena productiva.
                        </p>
                </div>
                <br />
                <div  class="primary-white-button">
                    <a href="#" class="scroll-link lang" data-id="about" key="conocer"><b>CONOCER MÁS</b></a>
                </div>
            </div>
        </div>
    </div>


    <section id="about" class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 fade">
                    <div class="service-item">
                        <div class="icon">
                            <img src="assets/img/service_icon_03.png" alt="">
                        </div>
                        <h1 style="color: white" class="lang" key="historia">Historia</h1>
                        <div class="line-dec"></div>
                        <br/>
                        <div id="historia">
                            <p style="font-size: 20px; line-height: 2; padding-left 5%; padding-right: 1%" class="fade lang" key="historia1">Nunkui es una empresa iniciada por jóvenes en el año 2019, partiendo de la premisa de servir, de la mano del cuidado de nuestro mundo, NUNKUI, es el nombre que desde la cosmovisión SHUAR, tiene la creadora de las plantas, se le atribuye la fertilidad de los suelos y del vientre de las mujeres.</p>
                            <p style="font-size: 20px; line-height: 2; padding-left: 2%; padding-right: 2%" class="fade lang" key="historia3">Nunkui no busca ser un negocio convencional, busca desarrollar un modelo económico sostenible, entregando el mejor producto para nuestros nuestros clientes, también difundiendo nuestra identidad cultural.</p>
                        </div>
                        <div class="primary-blue-button">
                            <a href="#" class="scroll-link lang" data-id="about1" key="mas_informacion">Más Información</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="about1" class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div id="nosotros" class="service-item ">
                        <div class="icon">
                            <img src="assets/img/service_icon_04.png" alt="">
                        </div>
                        <h4 style="font-size: 30px" class="lang" key="mision1">Misión</h4>
                        <div class="line-dec"></div>
                        <p id="historia1" class="fade" style="font-size: 18px">Cultivar frutas con métodos amigables al ambiente que sean de calidad, frescos y distribuirlos a nuestros clientes con excelente atención para llevar bienestar y salud a todas sus familias.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div id="nosotros" class="service-item ">
                        <div class="icon">
                            <img src="assets/img/service_icon_01.png" alt="">
                        </div>
                        <h4 style="font-size: 30px" class="lang" key="vision3">Visión</h4>
                        <div class="line-dec"></div>
                        <p id="historia1" class="fade" style="font-size: 18px"><span style="font-size: 18px" class="lang" key="vision">Ser una distribuidora frutal elegida por preocuparse en la salud, en una</span><br><span style="font-size: 18px" class="lang" key="vision1">alimentación nutritiva, por cuidar el medio ambiente y brindar la mejor fruta</span><br><span style="font-size: 18px" class="lang" key="vision2">para el Ecuador y el Mundo.</span></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div id="nosotros" class="service-item ">
                        <div class="icon">
                            <img src="assets/img/service_icon_02.png" alt="">
                        </div>
                        <h4 style="font-size: 30px; margin-bottom: 13px; margin-top: 8%" ><span class="lang" key="valores">Valores</span><br><span class="lang" key="valores1">Organizacionales</span></h4>
                        <div class="line-dec"></div>
                        <ul id="historia1" class="fade" style="margin-top: 8.5%;margin-bottom: 2%">
                            <li style="color: white; font-size: 18px" class="lang" key="valor1">- Responsabilidad</li>
                            <li style="color: white; margin-top: 5px; font-size: 18px" class="lang" key="valor2">- Respeto Ambiental</li>
                            <li style="color: white; margin-top: 5px; font-size: 18px" class="lang" key="valor3">- Compromiso</li>
                            <li style="color: white; margin-top: 5px; font-size: 18px" class="lang" key="valor4">- Identidad Ecologica</li>
                            <li style="color: white; margin-top: 5px; font-size: 18px" class="lang" key="valor5">- Altruismo Natural</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="logo">
                        <a class="logo-ft scroll-top" href="#"><em>Nun</em>kui</a>
                        <p>Copyright &copy; 2020 Nunkui</p>
                        <br>
                        <img alt="" src="https://connectamericas.com/sites/default/files/content-idb/verifiedbadge.png" />
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="location">
                        <h4 class="lang" key="menu">Menú</h4>
                        <ul>
                            <li><a href="/" class="lang" key="home" id="ini">Inicio</a></li>
                            <li><a href="/productos" class="lang" key="product">Productos</a></li>
                            <li><a href="/galeria" class="lang" key="galery">Galeria</a></li>
                            <li><a href="/quienes-somos" class="lang" key="who">Quienes Somos</a></li>
                            <li><a href="/contacto" class="lang" key="contact">Contacto</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="contact-info">
                        <h4 class="lang" key="contact">Contacto</h4>
                        <ul>
                            <li><i class="fa fa-globe mb-2"><a> Salcedo - Ecuador</a></i></li>
                            <li><i class="fa fa-map-marker mb-2"><a style="line-height: 1.5"> Juan León Mera & Guillermo Pacheco</a></i></li>
                            <li><i class="fa fa-user mb-2"><a> Sebastián Castro</a></i></li>
                            <li><i class="fa fa-envelope-o mb-2"><a> info@nunkui.com.ec</a></i></li>
                            <li><i class="fa fa-phone mb-2"><a> +593-99-292-3305</a></i></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="connect-us">
                        <h4 class="lang" key="redes">ENCUENTRANOS EN</h4>
                        <ul>
                            <li><a target="_blank" href="https://www.facebook.com/NunkuiFoods/"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/nunkuifoods/?hl=es-la"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=593992923305&text=Hola Nunkui acabo de revisar tu página web y me interesan tus productos"><i class="fa fa-whatsapp"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="assets/js/vendor/bootstrap.min.js"></script>

    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

    <script type="text/javascript" src="{{ asset("assets/js/idioma.js")}}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>

    
    <script type="text/javascript">
    $(document).ready(function() {
        // navigation click actions
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 750);
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 50;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
    $(function() {
        var selectedClass = "";
        $(".filter").click(function(){
        selectedClass = $(this).attr("data-rel");
        $("#gallery").fadeTo(100, 0.1);
        $("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
        setTimeout(function() {
        $("."+selectedClass).fadeIn().addClass('animation');
        $("#gallery").fadeTo(300, 1);
        }, 300);
        });
    });
    </script>
</body>
</html>
