<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--
Tinker Template
http://www.templatemo.com/tm-506-tinker
-->
        <title>Nunkui</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Yanone+Kaffeesatz:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
        <link href="assets/img/favicon.png" rel="icon">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/fontAwesome.css">
        <link rel="stylesheet" href="assets/css/hero-slider.css">
        <link rel="stylesheet" href="assets/css/owl-carousel.css">
        <link rel="stylesheet" href="assets/css/templatemo-style.css">
        <link rel="stylesheet" href="assets/css/lightbox.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

<body style="overflow-x: none">
    <style>
        .baner-content {
            width: 100%;
            padding-top: 35vh;
            text-align: center;
            background-image: url(assets/img/1-01.png);
            vertical-align: middle;
        }
        #productot1 {
            display: none;
        }

        #productd1 {
            display: none;
        }
        #idio{
            width: 4px
        }
        #idio1{
            width: 4px;
        }
        @media(max-width:720px){
            #productot1 {
                display: flex;
                color: #fff;
                font-weight: 500;
                letter-spacing: 0.5px;
                margin-left: 10%;
                margin-bottom: 5px;
            }
            #productot {
                display: none;
            }
            #productd{
                display: none
            }
            #productd1{
                display: flex;
                padding: 0px 20px;
            }
        }
    </style>
    <div class="header">
        <div class="container">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" style="font-family: Yanone Kaffeesatz !important;" class="navbar-brand scroll-top"><em style="font-family: Yanone Kaffeesatz !important;">Nun</em>kui</a>
                </div>
                <!--/.navbar-header-->
                <div id="main-nav" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/"><b class="lang" key="home">Inicio</b></a></li>
                        <li><a href="/productos"><b class="lang" key="product">Productos</b></a></li>
                        <li><a href="/galeria"><b class="lang" key="galery">Galeria</b></a></li>
                        <li><a href="/quienes-somos"><b class="lang" key="who">Quienes Somos</b></a></li>
                        <li><a href="/contacto"><b class="lang" key="contact">Contacto</b></a></li>
                        <li><img class="translate lang" key="es" id="es" src="assets/img/esp.png"></li>
                        <li><img class="translate lang" key="en" id="en" src="assets/img/eu.png"></li>
                    </ul>
                </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
        <!--/.container-->
    </div>
    <!--/.header-->
    <div class="parallax-content baner-content" id="home" >
        <div class="container">
            <div class="service-item">
                <h2 style="font-family: Yanone Kaffeesatz !important; font-size: 65px; color: #fff" ><span class="lang" key="pitahaya1" style="font-family: Yanone Kaffeesatz !important; color: #fff">PITA</span><span style="font-family: Yanone Kaffeesatz !important; color: #33c1cf" class="lang" key="pitahaya2">HAYA</span></h2>
                <div class="line-dec" style="border-bottom: 2px solid white"></div>
                <br />
                <p class="lang" key="pitahaya" style="font-size: 28px; line-height: 140%; color: white">¡La pitahaya es el alimento de moda entre los amantes de la comida saludable!</p>
                <br />
                <div class="primary-blue-button">
                    <a href="#" class="scroll-link lang"  key="leer" data-id="portfolio">LEER MÁS</a>
                </div>
            </div>
        </div>
    </div>

    <section id="portfolio">
        <div class="content-wrapper">
            <div class="inner-container container">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="section-heading">
                            <h4 id="productot" style="font-size: 47px"><b class="lang" key="pitahaya3">PITAHAYA AMARILLA</b></h4>
                            <h4 id="productot1" style="font-size: 25px; text-align: center"><b class="lang" key="pitahaya3">PITAHAYA AMARILLA</b></h4>
                            <div class="line-dec"></div>
                            <p class="lang" key="pitahayadesc" id="productd" style="font-size: 25px; line-height: 1.5;">Tiene una gran cantidad de beneficios y propiedades, su sabor es dulce, su olor es muy agradable y fácil de reconocer que no puedes dejar pasar la oportunidad de probarla.</p>
                            <p class="lang" key="pitahayadesc" id="productd1" style="font-size: 17px; line-height: 1.5;">Tiene una gran cantidad de beneficios y propiedades, su sabor es dulce, su olor es muy agradable y fácil de reconocer que no puedes dejar pasar la oportunidad de probarla.</p>
                            <div class="filter-categories ">
                                <ul class="tabs clearfix project-filter" data-tabgroup="first-tab-group">
                                    <li class="filter" data-filter="all"><a href="#tab1" class="active lang" key="todos">Todos</a></li>
                                    <li class="filter" data-filter="beneficios"><a href="#tab2" class="lang" key="beneficios" >Beneficios</a></li>
                                    <li class="filter" data-filter="consumo"><a href="#tab3" class="lang" key="consumo">Consumo</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="projects-holder-3">
                            <div class="projects-holder">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 project-item mix consumo">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/3-01.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="consumo1">Consumo</span><br><span class="lang" key="consumo1-1"></span><br><span class="lang" key="consumo1-2">Directo</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/rsz_7-01.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios1">Ayuda</span><br><span class="lang" key="beneficios1-1">a regular</span><br><span class="lang" key="beneficios1-2">tu peso</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/Sin titulo-4.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios2">Cuida</span><br><span class="lang" key="beneficios2-1">tu</span><br><span class="lang" key="beneficios2-2">Corazón</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic consumo">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/8-01.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="consumo2">Cócteles</span><br><span class="lang" key="consumo2-1">&</span><br><span class="lang" key="consumo2-2">Batidos</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic consumo">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/pastel.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="consumo3"></span><br><span class="lang" key="consumo3-1">Pasteles</span><br><span class="lang" key="consumo3-2"></span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/piel.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios3">Estabiliza</span><br><span class="lang" key="beneficios3-1">tus niveles</span><br><span class="lang" key="beneficios3-2">de azucar</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/proceso.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios4">Reduce el</span><br><span class="lang" key="beneficios4-1">riesgo de</span><br><span class="lang" key="beneficios4-2">presión Alta</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic consumo">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/ensalda.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="consumo4">ENSALADAS</span><br><span class="lang" key="consumo4-1">&</span><br><span class="lang" key="consumo4-2">GUARNICIONES</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic consumo">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/helado-min.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="consumo5"></span><br><span class="lang" key="consumo5-1">Helados</span><br><span class="lang" key="consumo5-2"></span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/digestion-min.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios5">Mejora</span><br><span class="lang" key="beneficios5-1">tu</span><br><span class="lang" key="beneficios5-2">digestión</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/envejecimiento-min.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios">Restrasa el</span><br><span class="lang" key="beneficios6-1">envejecimiento</span><br><span class="lang" key="beneficios6-2">celular</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/colageno-min.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios7">Promueve la</span><br><span class="lang" key="beneficios7-1">producción</span><br><span class="lang" key="beneficios7-2">de colágeno</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/inmune-min.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios8">Fortalece</span><br><span class="lang" key="beneficios8-1">el sistema</span><br><span class="lang" key="beneficios8-2">inmune</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-md-6 col-sm-6 project-item mix graphic beneficios">
                                        <figure class="snip0015" ontouchstart>
                                            <img src="assets/img/alergias-min.png" alt="sample38"/>
                                            <figcaption>
                                                <h2 style="line-height: 1.5; word-spacing: 5px"><span class="lang" key="beneficios9">Ayuda a</span><br><span class="lang" key="beneficios9-1">resistir las</span><br><span class="lang" key="beneficios9-2">alergias</span></h2>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="logo">
                        <a class="logo-ft scroll-top" href="#"><em>Nun</em>kui</a>
                        <p>Copyright &copy; 2020 Nunkui</p>
                        <br>
                        <img alt="" src="https://connectamericas.com/sites/default/files/content-idb/verifiedbadge.png" />
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="location">
                        <h4 class="lang" key="menu">Menú</h4>
                        <ul>
                            <li><a href="/" class="lang" key="home" id="ini">Inicio</a></li>
                            <li><a href="/productos" class="lang" key="product">Productos</a></li>
                            <li><a href="/galeria" class="lang" key="galery">Galeria</a></li>
                            <li><a href="/quienes-somos" class="lang" key="who">Quienes Somos</a></li>
                            <li><a href="/contacto" class="lang" key="contact">Contacto</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="contact-info">
                        <h4 class="lang" key="contact">Contacto</h4>
                        <ul>
                            <li><i class="fa fa-globe mb-2"><a> Salcedo - Ecuador</a></i></li>
                            <li><i class="fa fa-map-marker mb-2"><a style="line-height: 1.5"> Juan León Mera & Guillermo Pacheco</a></i></li>
                            <li><i class="fa fa-user mb-2"><a> Sebastián Castro</a></i></li>
                            <li><i class="fa fa-envelope-o mb-2"><a> info@nunkui.com.ec</a></i></li>
                            <li><i class="fa fa-phone mb-2"><a> +593-99-292-3305</a></i></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="connect-us">
                        <h4 class="lang" key="redes">ENCUENTRANOS EN</h4>
                        <ul>
                            <li><a target="_blank" href="https://www.facebook.com/NunkuiFoods/"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/nunkuifoods/?hl=es-la"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=593992923305&text=Hola Nunkui acabo de revisar tu página web y me interesan tus productos"><i class="fa fa-whatsapp"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="assets/js/vendor/bootstrap.min.js"></script>

    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

    <script type="text/javascript" src="{{ asset("assets/js/idioma.js")}}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        document.getElementById('b1').style.display = "none";
    });
    $(document).ready(function() {
        // navigation click actions
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 750);
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 50;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
    </script>
</body>
</html>
