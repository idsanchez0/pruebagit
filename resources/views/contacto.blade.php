<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--
Tinker Template
http://www.templatemo.com/tm-506-tinker
-->
        <title>Nunkui</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Yanone+Kaffeesatz:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">

        <link href="assets/img/favicon.png" rel="icon">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/fontAwesome.css">
        <link rel="stylesheet" href="assets/css/hero-slider.css">
        <link rel="stylesheet" href="assets/css/owl-carousel.css">
        <link rel="stylesheet" href="assets/css/templatemo-style.css">
        <link rel="stylesheet" href="assets/css/lightbox.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            .baner-content {
                width: 100%;
                padding-top: 35vh;
                text-align: center;
                background-image: url(assets/img/rsz_2-01_1.png);
                vertical-align: middle;
            }
        </style>
    </head>
<body>
    <style>
        .white-black{
            background-color: rgba(0, 0, 0, 0.5) !important;
        }
        #blogs {
            padding: 133.5px 0px;
            background-image: url(assets/img/rsz_1-01.png);
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
        label.error {
            color: white;
            display: block;
            margin-top: 5px;
        }
    </style>
    <div class="header">
        <div class="container">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" style="font-family: Yanone Kaffeesatz !important;" class="navbar-brand scroll-top"><em style="font-family: Yanone Kaffeesatz !important;">Nun</em>kui</a>
                </div>
                <!--/.navbar-header-->
                <div id="main-nav" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/"><b class="lang" key="home">Inicio</b></a></li>
                        <li><a href="/productos"><b class="lang" key="product">Productos</b></a></li>
                        <li><a href="/galeria"><b class="lang" key="galery">Galeria</b></a></li>
                        <li><a href="/quienes-somos"><b class="lang" key="who">Quienes Somos</b></a></li>
                        <li><a href="/contacto"><b class="lang" key="contact">Contacto</b></a></li>
                        <li><img class="translate lang" key="es" id="es" src="assets/img/esp.png"></li>
                        <li><img class="translate lang" key="en" id="en" src="assets/img/eu.png"></li>
                    </ul>
                </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
        <!--/.container-->
    </div>
    <!--/.header-->
    <div class="parallax-content baner-content" id="home" >
        <div class="container">
            <div class="text-content">
                <h2 style="font-family: Yanone Kaffeesatz !important; font-size: 65px; color: #fff"><a style="font-family: Yanone Kaffeesatz !important; color: white; text-decoration: none" class="lang" key="contacto">CONT</a><span style="font-family: Yanone Kaffeesatz !important; color: #33c1cf" class="lang" key="contacto1">ACTO</span></h2>
                <br />
                <p style="font-size: 22px; line-height: 140%" class="lang" key="contacto2">Para obtener más información de nuestros productos puedes dar clic en el botón contactar</p>
                <br />
                <div class="primary-white-button">
                    <a href="#" class="scroll-link lang" id="btn" data-id="blogs" key="contacto3"><b>Contactar</b></a>
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-content "  id="blogs">
        <div class="container">
            <div class="row">
                <div class="wrapper">
                    <div id="map" class="col-md-6 fade">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.1586056647243!2d-78.58691308524627!3d-1.0419457992399506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMcKwMDInMzEuMCJTIDc4wrAzNScwNS4wIlc!5e0!3m2!1ses!2sec!4v1596063413744!5m2!1ses!2sec" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                        
                    </div>
                    <div class="col-md-6 fade">
                        <div class="container" style="width: 100%; background-color: rgba(29, 28, 28, 0.5)">
                            <h1 style="color: white" class="lang" key="info"><b>Información</b></h1>
                            <form id="contact" action="{{ route('contact-mail') }}" data-toggle="validator" novalidate="novalidate" method="post" role="form">
                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                      <fieldset>
                                        <input name="nombre" type="text" maxlength="50" class="form-control" id="nombre" placeholder="Name" onkeypress="return soloLetras(event)" required>
                                      </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                      <fieldset>
                                        <input name="email" type="text" class="form-control" id="email" placeholder="Email" required>
                                      </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset>
                                          <input name="telefono" type="text" maxlength="10" class="form-control" id="telefono" placeholder="Phone" required onkeypress="return soloNumeros(event)">
                                          <input name="fecha" type="text" class="form-control" id="fecha" style="display: none" >
                                        </fieldset>
                                      </div>
                                    <div class="col-md-12">
                                      <fieldset>
                                        <textarea name="mensaje" style="height: 110px" class="form-control" id="mensaje" placeholder="Message" required></textarea>
                                      </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                      <fieldset>
                                        <button type="submit"  id="form-submit" class="btn lang" onclick="fecha1();" key="mensaje">Enviar Mensaje</button>
                                      </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="logo">
                        <a class="logo-ft scroll-top" href="#"><em>Nun</em>kui</a>
                        <p>Copyright &copy; 2020 Nunkui</p>
                        <br>
                        <img alt="" src="https://connectamericas.com/sites/default/files/content-idb/verifiedbadge.png" />
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="location">
                        <h4 class="lang" key="menu">Menú</h4>
                        <ul>
                            <li><a href="/" class="lang" key="home" id="ini">Inicio</a></li>
                            <li><a href="/productos" class="lang" key="product">Productos</a></li>
                            <li><a href="/galeria" class="lang" key="galery">Galeria</a></li>
                            <li><a href="/quienes-somos" class="lang" key="who">Quienes Somos</a></li>
                            <li><a href="/contacto" class="lang" key="contact">Contacto</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="contact-info">
                        <h4 class="lang" key="contact">Contacto</h4>
                        <ul>
                            <li><i class="fa fa-globe mb-2"><a> Salcedo - Ecuador</a></i></li>
                            <li><i class="fa fa-map-marker mb-2"><a style="line-height: 1.5"> Juan León Mera & Guillermo Pacheco</a></i></li>
                            <li><i class="fa fa-user mb-2"><a> Sebastián Castro</a></i></li>
                            <li><i class="fa fa-envelope-o mb-2"><a> info@nunkui.com.ec</a></i></li>
                            <li><i class="fa fa-phone mb-2"><a> +593-99-292-3305</a></i></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="connect-us">
                        <h4 class="lang" key="redes">ENCUENTRANOS EN</h4>
                        <ul>
                            <li><a target="_blank" href="https://www.facebook.com/NunkuiFoods/"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/nunkuifoods/?hl=es-la"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=593992923305&text=Hola Nunkui acabo de revisar tu página web y me interesan tus productos"><i class="fa fa-whatsapp"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="{{ asset("asstes/js/fade.js")}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="assets/js/vendor/bootstrap.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script type="text/javascript" src="{{ asset("assets/js/idioma.js")}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="{{ asset("assets/js/mail.js")}}"></script>
</body>
</html>
