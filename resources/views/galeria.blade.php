<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--
Tinker Template
http://www.templatemo.com/tm-506-tinker
-->
        <title>Nunkui</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Yanone+Kaffeesatz:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">

        <link href="assets/img/favicon.png" rel="icon">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/fontAwesome.css">
        <link rel="stylesheet" href="assets/css/hero-slider.css">
        <link rel="stylesheet" href="assets/css/owl-carousel.css">
        <link rel="stylesheet" href="assets/css/templatemo-style.css">
        <link rel="stylesheet" href="assets/css/lightbox.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            .baner-content {
                width: 100%;
                padding-top: 35vh;
                text-align: center;
                background-image: url(assets/img/galeria.png);
                vertical-align: middle;
            }
            #about {
                padding: 150px 0px;
                background-image: url(assets/img/galeria1.png);
                background-repeat: no-repeat;
                background-size: cover;
                background-attachment: fixed;
            }
            #menos{
                display: none;
            }
            #mas-fotos{
                display: none;
            }
        </style>
    </head>

<body>
    <style>
        .news-tile .card-title {
            position: absolute;
            bottom: 15px;
            left: 40px;
        }
        .rgba-stylish-strong {
            background-color: rgba(62, 69, 81, .5);
        }
        .text-white {
            color: #fff!important;
        }
        .gallery {
            -webkit-column-count: 3;
            -moz-column-count: 3;
            column-count: 3;
            -webkit-column-width: 33%;
            -moz-column-width: 33%;
            column-width: 33%;
        }
        .gallery .pics {
            -webkit-transition: all 350ms ease;
            transition: all 350ms ease; }
            .gallery .animation {
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
        }
        #idio{
            width: 4px
        }
        #idio1{
            width: 4px;
        }
        @media (max-width: 450px) {
            .gallery {
                -webkit-column-count: 1;
                -moz-column-count: 1;
                column-count: 1;
                -webkit-column-width: 100%;
                -moz-column-width: 100%;
                column-width: 100%;
            }
        }
        @media (max-width: 400px) {
            .btn.filter {
                padding-left: 1.1rem;
                padding-right: 1.1rem;
            }
        }
    </style>
    <div class="header">
        <div class="container">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" style="font-family: Yanone Kaffeesatz !important;" class="navbar-brand scroll-top"><em style="font-family: Yanone Kaffeesatz !important;">Nun</em>kui</a>
                </div>
                <!--/.navbar-header-->
                <div id="main-nav" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/"><b class="lang" key="home">Inicio</b></a></li>
                        <li><a href="/productos"><b class="lang" key="product">Productos</b></a></li>
                        <li><a href="/galeria"><b class="lang" key="galery">Galeria</b></a></li>
                        <li><a href="/quienes-somos"><b class="lang" key="who">Quienes Somos</b></a></li>
                        <li><a href="/contacto"><b class="lang" key="contact">Contacto</b></a></li>
                        <li><img class="translate lang" key="es" id="es" src="assets/img/esp.png"></li>
                        <li><img class="translate lang" key="en" id="en" src="assets/img/eu.png"></li>
                    </ul>
                </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
        <!--/.container-->
    </div>
    <!--/.header-->
    <div class="parallax-content baner-content" id="home">
        <div class="container">
            <div class="text-content">
                <h2 style="font-family: Yanone Kaffeesatz !important; font-size: 65px; color: #fff"><a style="font-family: Yanone Kaffeesatz !important; color: white; text-decoration: none" class="lang" key="galeria">GALERIA</a><span style="font-family: Yanone Kaffeesatz !important; color: #33c1cf" class="lang" key="imagen">IMÁGENES</span></h2>
                <br>
                <div class="primary-white-button">
                    <a href="#" class="scroll-link lang" data-id="about" key="galeria1">Galeria</a>
                </div>
            </div>
        </div>
    </div>


    <section id="about" class="page-section">
        <div class="container">
            <div class="row" id="g1">
                <div class="gallery fade" id="gallery">
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 2 ">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/piel.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 1 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria2.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 1 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria3.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 2 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria4.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 2 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria5.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 1 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria6.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                  </div>
            </div>
            <br>
            <div class="row">
                <center>
                    <div class="primary-blue-button">
                        <a href="#" style="width: 100px" class="scroll-link lang" onclick="more();" data-id="espacio" id="mas" key="mas">Ver Más</a>
                    </div>
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <div class="primary-blue-button" id="espacio">

                    </div>
                </center>
            </div>
            <br>
            <div class="row" id="mas-fotos">
                <div class="gallery fade" id="gallery">
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 2 ">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria7.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 1 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria8.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 1 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria9.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 2 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria10.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 2 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria11.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class="mb-3 news-tile pics animation all 1 espacio">
                        <a ontouchstart class="text-white over">
                            <img class="img-fluid" src="assets/img/galeria/galeria12.png" alt="Card image cap">
                            <div class="mask rgba-stylish-strong">
                                <div class="text-white py-lg-5 py-0 my-0">
                                    <div>
                                        <h4 class="card-title font-weight-bold pt-2">
                                            <strong></strong>
                                        </h4>
                                        <!--<p class="mx-5 clearfix d-none d-md-block"></p>-->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Grid column -->
                  </div>
            </div>
            <br>
            <div class="row">
                <center>
                    <div class="primary-blue-button">
                        <a href="#" style="width: 100px" class="scroll-link lang" onclick="nomore();" data-id="about" id="menos" key="menos">Menos</a>
                    </div>
                </center>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="logo">
                        <a class="logo-ft scroll-top" href="#"><em>Nun</em>kui</a>
                        <p>Copyright &copy; 2020 Nunkui</p>
                        <br>
                        <img alt="" src="https://connectamericas.com/sites/default/files/content-idb/verifiedbadge.png" />
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="location">
                        <h4 class="lang" key="menu">Menú</h4>
                        <ul>
                            <li><a href="/" class="lang" key="home" id="ini">Inicio</a></li>
                            <li><a href="/productos" class="lang" key="product">Productos</a></li>
                            <li><a href="/galeria" class="lang" key="galery">Galeria</a></li>
                            <li><a href="/quienes-somos" class="lang" key="who">Quienes Somos</a></li>
                            <li><a href="/contacto" class="lang" key="contact">Contacto</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="contact-info">
                        <h4 class="lang" key="contact">Contacto</h4>
                        <ul>
                            <li><i class="fa fa-globe mb-2"><a> Salcedo - Ecuador</a></i></li>
                            <li><i class="fa fa-map-marker mb-2"><a style="line-height: 1.5"> Juan León Mera & Guillermo Pacheco</a></i></li>
                            <li><i class="fa fa-user mb-2"><a> Sebastián Castro</a></i></li>
                            <li><i class="fa fa-envelope-o mb-2"><a> info@nunkui.com.ec</a></i></li>
                            <li><i class="fa fa-phone mb-2"><a> +593-99-292-3305</a></i></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="connect-us">
                        <h4 class="lang" key="redes">ENCUENTRANOS EN</h4>
                        <ul>
                            <li><a target="_blank" href="https://www.facebook.com/NunkuiFoods/"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/nunkuifoods/?hl=es-la"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=593992923305&text=Hola Nunkui acabo de revisar tu página web y me interesan tus productos"><i class="fa fa-whatsapp"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="assets/js/vendor/bootstrap.min.js"></script>

    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

    <script type="text/javascript" src="{{ asset("assets/js/idioma.js")}}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        // navigation click actions
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 750);
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 50;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
    $(function() {
        var selectedClass = "";
        $(".filter").click(function(){
        selectedClass = $(this).attr("data-rel");
        $("#gallery").fadeTo(100, 0.1);
        $("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
        setTimeout(function() {
        $("."+selectedClass).fadeIn().addClass('animation');
        $("#gallery").fadeTo(300, 1);
        }, 300);
        });
    });
    </script>
</body>
</html>
