<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
    //
    public $successStatus = 200;
    public function store(Request $request){
        Mail::send('emails', $request->all(), function($msj){
            $msj->subject('Correo de contacto');
            $msj->to('info@nunkui.com.ec');
        });
        return response()->json(['success'=>'registro exitoso'],$this->successStatus);
    }
}
